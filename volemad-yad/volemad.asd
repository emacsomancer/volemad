;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; volemad.asd

#+sb-core-compression
(defmethod asdf:perform ((o asdf:program-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression 9))

(asdf:defsystem #:volemad
  :description "A responsive frontend for Mullvad WireGuard interaction."
  :author "Benjamin Slade <slade@lambda-y.net>"
  :license "GPLv3"
  :depends-on (#:alexandria
               #:drakma
               #:jsown
               #:yason
               #:bordeaux-threads)
  :serial t
  :components ((:file "volemad"))
  :build-operation "program-op"
  ;; :defsystem-depends-on (#:deploy) ;; https://github.com/Shinmera/deploy
  ;; :build-operation "deploy-op"
  :build-pathname "volemad"
  :entry-point "volemad:launch")
