;;; volemad.lisp --- systray interface for Mullvad WireGuard connections

;; Copyright (C) 2020 Benjamin Slade

;; LICENCE:
;; This program is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; This program is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
;; PARTICULAR PURPOSE. See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License along with this
;; program. If not, see http://www.gnu.org/licenses/.

(defpackage :volemad
  (:use :common-lisp)
  (:export #:launch))
(in-package :volemad)

;; get user directory
(defparameter *userhome* (uiop:getenv "HOME"))

;; determine icon locations
(defparameter *volemad-connected-icon* 'nil)
(defparameter *volemad-disconnected-icon* 'nil)
(if (probe-file "/usr/share/icons/volemad/volemad-connected.png")
    (progn
      (setf *volemad-connected-icon* "/usr/share/icons/volemad/volemad-connected.png")
      (setf *volemad-disconnected-icon* "/usr/share/icons/volemad/volemad-disconnected.png"))
    (progn
      (setf *volemad-connected-icon* "/usr/local/share/icons/volemad/volemad-connected.png")
      (setf *volemad-disconnected-icon* "/usr/local/share/icons/volemad/volemad-disconnected.png")))

;; get Mullvad data
(defvar *mullvad-data* 'nil)

(defun mullvad-parse-json ()
  "Parse JSON data from Mullvad."
  (let ((json-string (map 'string 'code-char
                          (drakma:http-request
                           "https://api.mullvad.net/public/relays/wireguard/v1/"))))
    (let* ((jsparse (jsown:parse json-string))
           (jscountries (jsown:val jsparse "countries"))
           (servers 'nil)
           (cities-servers 'nil))
      (setf *mullvad-data*
            (loop for country in jscountries
                  do (let ((citylist (jsown:val country "cities")))
                       (setf cities-servers
                             (loop for city in citylist
                                   do (let ((serverlist (jsown:val city "relays")))
                                        (setf servers
                                              (loop for server in serverlist
                                                    collect (jsown:val server "hostname"))))
                                   collect (cons (jsown:val city "name") (list servers)))))
                  collect (cons (jsown:val country "name") (list cities-servers)))))))

;; set up yad menu
(defvar *yad-country-connect* 'nil)

(defun message-daemon (action)
  (let* ((location (uiop:read-file-lines "/tmp/volemad-loc"))
         (to-daemon (open (car location) :direction :output :if-exists :append)))
    (write-line action to-daemon)
    (force-output)
    (finish-output to-daemon)
    (close to-daemon)
    ;; (force-output (uiop:process-info-input to-daemon))
    ))

(defun yad-create-menu ()
  "Construct menu to send to Yad interface."
  ;; define 'DISCONNECT' menu item for yad
  (setf *yad-country-connect*
        (concatenate 'string
                     "DISCONNECT ! echo 'DISCONNECT'|---------------------------------|"
                     "RANDOM server ! echo 'RANDOM'|---------------------------------|"))

  ;; create Yad menu items for different Mullvad relays/servers
  (loop for country in *mullvad-data*
        do (if (> (length (cadr country)) 1) ;; for countries with only one city
               (progn
                 (setf *yad-country-connect*
                       (concatenate 'string *yad-country-connect*
                                    (string-upcase (car country))
                                    " ! echo '" (car country) "'|"))
                 (loop for city in (cadr country)
                       do (loop for server in (cdr city)
                                collect (car server))
                          (setf *yad-country-connect*
                                (concatenate 'string
                                             *yad-country-connect*
                                             "  " (car city) " ! echo '" 
                                             (car country) "^"
                                             (car city) "'|"))))
               (progn                        ;; for countries with multiple cities
                 (setf *yad-country-connect*
                       (concatenate 'string *yad-country-connect*
                                    (string-upcase (car country)) ": "))
                 (loop for city in (cadr country)
                       do (loop for server in (cdr city)
                                collect (car server))
                          (setf *yad-country-connect*
                                (concatenate 'string
                                             *yad-country-connect*
                                             (car city) "! echo '"
                                             (car country) "'|"))))))

  ;; define 'QUIT' menu item for Yad menu
  (setf *yad-country-connect*
        (concatenate 'string *yad-country-connect*
                     "|---------------------------------|QUIT ! echo 'QUIT'||")))

;; like Emacs' executable-find
(defun executable-find (exec)
  "Determine if EXEC is an executable program on system."
  (let ((executable-exists
          (string-trim '(#\Newline #\") (with-output-to-string (s)
                                          (uiop:run-program
                                           (concatenate 'string
                                                        "sh -c \"command -v " exec " 2>&1 \"")
                                           :output s :ignore-error-status t)))))
    (if (not (string= executable-exists ""))
        executable-exists
        nil)))

(defun setup-yad ()
  "Launch yad and open stream for communication."
  (defparameter *yad*
    (when (executable-find "yad")
      (uiop:launch-program
       (concatenate 'string "yad --notification "
                    "--menu=\"" *yad-country-connect* "\" "
                    "--image='" *volemad-disconnected-icon* "' "
                    "--command=\"echo 'GET-LOCATION'\" "
                    "--text='Volemad initialising...' "
                    "--listen")
       :input :stream :output :stream))))

(defun assocvalue (inp alist)
  "Helper function to get the 'value' of a 'key' (INP) in an assoc ALIST."
  (cdr (assoc inp alist :test #'string=)))

(defun find-ip-ipapi ()
  "Get information about current (apparent) ip location."
  (let* ((ip (string-trim '(#\Newline #\")
                          (write-to-string
                           (drakma:http-request "https://ipinfo.io/ip"))))
         (stream (drakma:http-request
                  (concatenate 'string "https://ipapi.co/" ip "/json")
                  :want-stream t))
         (geoinfo 'nil)
         (json-error 'nil))
    (setf (flexi-streams:flexi-stream-external-format stream) :utf-8)
    ;; (ignore-errors
    (handler-case 
        (setf geoinfo (yason:parse stream :object-as :alist))
      (error (c)
        (setf json-error 't)))
    (if json-error
        "Error: could not determine ip information"
        (let* ((country (assocvalue "country_name" geoinfo))
               (region (assocvalue "region" geoinfo))
               (city (assocvalue "city" geoinfo))
               (latitude (write-to-string (assocvalue "latitude" geoinfo)))
               (longitude (write-to-string (assocvalue "longitude" geoinfo)))
               (timezone (assocvalue "timezone" geoinfo))
               (ip (assocvalue "ip" geoinfo)))
          (concatenate 'string city ", " region ", " country "
[" timezone "]" "
(" latitude "," longitude ")
" ip)))))

(defun get-current-connection ()
  "Return the current connected Mullvad server (if connected)."
  (when (executable-find "wg")
    (let* ((string1
             (with-output-to-string (s)
               (uiop:run-program "wg show"
                                 :ignore-error-status t
                                 :error-output s))))
      (if (equal string1 "")
          'nil
          (string-trim '(#\Newline #\")
                       (subseq string1
                               (search "mullvad-" string1)
                               (search ": " string1
                                       :start2 (search "mullvad-" string1))))))))

;; create top-level variable to store the last known Mullvad connection
(defparameter *last-connection* "UPDATEME")

(defun mullvad-translate-relay (relay)
  "Translate 'xxx-wireguard' into 'mullvad-xxx'."
  (concatenate 'string "mullvad-"
               (subseq relay 0 (position #\- relay :test #'equal))))

(defun find-and-return-cdr (target list)
  "Helper function to get the cdr of TARGET in LIST."
  (if (equal list 'nil)
      'nil
      (let ((top (car list)))
        (if (equal (car top) target)
            (cadr top)
            (find-and-return-cdr target (cdr list))))))

(defun collect-all-relays-for-city (country city list)
  "In a LIST, collect all servers/relays for CITY in COUNTRY."
  (let ((cities (find-and-return-cdr country list)))
    (find-and-return-cdr city cities)))

(defun collect-all-relays-for-country (country list)
  "In a LIST, collect all servers/relays for (all cities) in COUNTRY."
  (alexandria:flatten
   (let ((cities (find-and-return-cdr country list)))
     (loop for city in cities
           collect (find-and-return-cdr (car city) cities)))))

(defun collect-all-relays-global (list)
  "In a LIST, collect all servers/relays for all cities in all countries."
  (alexandria:flatten
   (loop for country in *mullvad-data*
         collect (let ((cities (find-and-return-cdr (car country) list)))
                   (loop for city in cities
                         collect (find-and-return-cdr (car city) cities))))))

(defun find-location-of-relay (relay)
  "Find the country and city of a RELAY/server."
  (let ((location 'nil))
    (loop for country in *mullvad-data*
          do (loop for city in (cadr country)
                   do (let ((servers (collect-all-relays-for-city
                                      (car country)
                                      (car city)
                                      *mullvad-data*))) ;; master list
                        (loop for server in servers
                              do (when
                                     (string=
                                      (mullvad-translate-relay server) relay)
                                   (setf location (concatenate 'string
                                                               (car country)
                                                               ":"
                                                               (car city))))))))
    (concatenate 'string location " (" relay ")")))

(defun mullvad-disconnect (target-connection)
  "Disconnect from Wireguard TARGET-CONNECTION server/relay."
  ;; (when (executable-find "wg-quick")
  (message-daemon (concatenate 'string "down " target-connection)))
;; )

(defun test-blacklist ()
  "Test if currently connected server is blacklisted."
  (let* ((json-string (map 'string 'code-char
                           (drakma:http-request "https://am.i.mullvad.net/json")))
         (jsparse (jsown:parse json-string))
         (blacklist (jsown:val jsparse "blacklisted"))
         (blacklisted (jsown:val blacklist "blacklisted")))
    blacklisted))

(defun choose-random (list)
  "Choose a random member of LIST."
  (setf *random-state* (make-random-state t))
  (nth (random (length list)) list))

(defun mullvad-connect (servers)
  "Connect to one the Mullvad Wireguard SERVERS."
  ;; (when (executable-find "wg-quick")
    (let ((random-server (choose-random servers)))
      (message-daemon (concatenate 'string "up " random-server))
      (sleep 3)
      (let ((currconnection (get-current-connection)))
        (when (test-blacklist) ;; if connected server is blacklisted
          (mullvad-disconnect currconnection)
          ;; disconnect and try another server from the same pool, after
          ;; removing blacklisted server
          (mullvad-connect (remove currconnection servers :test #'string=))))))
;; )

(defun mullvad-action (input)
  "Given INPUT, decide which relays are appropriate to connect to."
  (let* ((currconnection (get-current-connection))
         (locations (uiop:split-string input :separator "^"))
         (country (car locations))
         (city (if (> (length locations) 1)
                   (cadr locations)
                   'nil))
         (relays
           (if (null country)
               (collect-all-relays-global *mullvad-data*)
               (if (null city)
                   (collect-all-relays-for-country country *mullvad-data*)
                   (collect-all-relays-for-city country city *mullvad-data*))))
         (mullvad-servers
           (loop for relay in relays
                 collect (mullvad-translate-relay relay))))
    (unless (null currconnection)
      (mullvad-disconnect currconnection))
    (mullvad-connect mullvad-servers)))

(defun update-tooltip ()
  "Appropriately update the icon and tooltip text of Yad systray applet."
  ;; (print "update-tooltip")
  (let ((daemon-running 't))
    (handler-case 
        (progn (uiop:run-program "pgrep volemad-daemon")
               (setf daemon-running 't))
      (error (c)
        (setf daemon-running 'nil)))
    (if (null daemon-running)
        (progn
          (write-line
           (concatenate 'string "icon:" *volemad-disconnected-icon*)
           (uiop:process-info-input *yad*))
          (write-line
           "tooltip:Volemad daemon not running" 
           (uiop:process-info-input *yad*))
          (force-output (uiop:process-info-input *yad*)))
        (let ((currconnection (get-current-connection)))
          ;; (unless (string= currconnection *last-connection*)
            (if (null currconnection)
                (progn
                  (write-line
                   (concatenate 'string "icon:" *volemad-disconnected-icon*)
                   (uiop:process-info-input *yad*))
                  (write-line
                   "tooltip:Mullvad disconnected" 
                   (uiop:process-info-input *yad*)))
                (progn
                  (write-line
                   (concatenate 'string "icon:" *volemad-connected-icon*)
                   (uiop:process-info-input *yad*))
                  (write-line
                   (concatenate 'string "tooltip:" (find-location-of-relay currconnection))
                   (uiop:process-info-input *yad*))))
            (force-output (uiop:process-info-input *yad*))
          (setf *last-connection* currconnection)))))
;; )

(defun menu-action (input)
  "Interpret INPUT from Yad menu commands."
  (unless (string= input "")
    (cond 
      ((string= input "QUIT")
       (uiop:close-streams *yad*))
      ((string= input "DISCONNECT")
       (let ((currconnection (get-current-connection)))
         (unless (null currconnection)
           (mullvad-disconnect currconnection))))
      ((string= input "GET-LOCATION")
       (when (executable-find "notify-send")
         (if (null (get-current-connection))
             (uiop:run-program
              (concatenate 'string
                           "notify-send 'Apparent location' '"
                           (find-ip-ipapi) "' --icon=" *volemad-disconnected-icon*))
             (uiop:run-program
              (concatenate 'string
                           "notify-send 'Apparent location' '"
                           (find-ip-ipapi) "' --icon=" *volemad-connected-icon*)))))
      ((string= input "RANDOM")
       (mullvad-action ""))
      (t (mullvad-action input)))))

(defun yad-tooltip-updater ()
  "Yad tooltip & icon update timer."
  (bt:make-thread
   (lambda ()
     (loop while t
           do (sleep 2)
              (update-tooltip)))))

(defun launch ()
  "Top-level function for running Volemad."
  (mullvad-parse-json)
  (yad-create-menu)
  (setup-yad)
  (yad-tooltip-updater)
  (let ((yad-input ""))
    (update-tooltip)
    (let ((stream (uiop:process-info-output *yad*)))
      (loop while (not (string= yad-input "QUIT")) 
            do (listen stream)
               (setf yad-input (read-line stream))
               (menu-action yad-input)
               (when (string= yad-input "QUIT")
                 (uiop:close-streams *yad*)
                 (bt:destroy-thread (yad-tooltip-updater))
                 (uiop:quit))))))

;; (launch)

;;  sbcl --eval "(ql:quickload :volemad)" --eval "(progn (in-package :volemad) (sb-ext:save-lisp-and-die \"volemad\" :toplevel #'volemad:launch :executable t :purify t))"
