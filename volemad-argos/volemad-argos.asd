;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; volemad.asd

#+sb-core-compression
(defmethod asdf:perform ((o asdf:program-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression 9))

(asdf:defsystem #:volemad-argos
  :description "An Argos-compatible frontend for Mullvad WireGuard interaction."
  :author "Benjamin Slade <slade@lambda-y.net>"
  :license "GPLv3"
  :depends-on (#:drakma
               #:jsown
               #:yason
               #:bordeaux-threads)
  :serial t
  :components ((:file "volemad-argos"))
  :build-operation "program-op" ;; leave as is
  :build-pathname "volemad-argos"
  :entry-point "volemad-argos:menu")
