;;; volemad-argos.lisp --- Mullvad WireGuard connection interface for Argos/GNOME Shell

;; Copyright (C) 2020 Benjamin Slade

;; LICENCE:
;; This program is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; This program is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
;; PARTICULAR PURPOSE. See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License along with this
;; program. If not, see http://www.gnu.org/licenses/.

;; (ql:quickload :drakma)
;; (ql:quickload :jsown)
;; (ql:quickload :yason)

(defpackage :volemad-argos
  (:use :common-lisp)
  (:export #:menu))

(in-package :volemad-argos)

;; get user directory
(defvar *userhome* (uiop:getenv "HOME"))

;; determine icon locations
(defparameter *volemad-connected-icon* 'nil)
(defparameter *volemad-disconnected-icon* 'nil)
(if (probe-file "/usr/share/icons/volemad/volemad-connected.png")
    (progn
      (setf *volemad-connected-icon* "/usr/share/icons/volemad/volemad-connected.png")
      (setf *volemad-disconnected-icon* "/usr/share/icons/volemad/volemad-disconnected.png"))
    (progn
      (setf *volemad-connected-icon* "/usr/local/share/icons/volemad/volemad-connected.png")
      (setf *volemad-disconnected-icon* "/usr/local/share/icons/volemad/volemad-disconnected.png")))

;; create top-level variable to store the last known Mullvad connection
(defvar *last-connection* "UPDATEME")
(defvar *argos-icon* 'nil)
(defvar *argos-status* 'nil)

;; get Mullvad data
(defvar *mullvad-data* 'nil)

(defun mullvad-parse-json ()
  "Parse JSON data from Mullvad."
  (let ((json-string (map 'string 'code-char
                          (drakma:http-request
                           "https://api.mullvad.net/public/relays/wireguard/v1/"))))
    (let* ((jsparse (jsown:parse json-string))
           (jscountries (jsown:val jsparse "countries"))
           (servers 'nil)
           (cities-servers 'nil))
      (setf *mullvad-data*
            (loop for country in jscountries
                  do (let ((citylist (jsown:val country "cities")))
                       (setf cities-servers
                             (loop for city in citylist
                                   do (let ((serverlist (jsown:val city "relays")))
                                        (setf servers
                                              (loop for server in serverlist
                                                    collect (jsown:val server "hostname"))))
                                   collect (cons (jsown:val city "name") (list servers)))))
                  collect (cons (jsown:val country "name") (list cities-servers)))))))

(defun get-current-connection ()
  "Return the current connected Mullvad server (if connected)."
  (when (executable-find "wg")
    (let* ((string1
             (with-output-to-string (s)
               (uiop:run-program "wg show"
                                 :ignore-error-status t
                                 :error-output s))))
      (if (equal string1 "")
          'nil
          (string-trim '(#\Newline #\")
                       (subseq string1
                               (search "mullvad-" string1)
                               (search ": " string1
                                       :start2 (search "mullvad-" string1))))))))

(defun mullvad-translate-relay (relay)
  "Translate 'xxx-wireguard' into 'mullvad-xxx'."
  (concatenate 'string "mullvad-"
               (subseq relay 0 (position #\- relay :test #'equal))))

(defun find-and-return-cdr (target list)
  "Helper function to get the cdr of TARGET in LIST."
  (if (equal list 'nil)
      'nil
      (let ((top (car list)))
        (if (equal (car top) target)
            (cadr top)
            (find-and-return-cdr target (cdr list))))))

(defun collect-all-relays-for-city (country city list)
  "In a LIST, collect all servers/relays for CITY in COUNTRY."
  (let ((cities (find-and-return-cdr country list)))
    (find-and-return-cdr city cities)))

(defun collect-all-relays-for-country (country list)
  "In a LIST, collect all servers/relays for (all cities) in COUNTRY."
  (alexandria:flatten
   (let ((cities (find-and-return-cdr country list)))
     (loop for city in cities
           collect (find-and-return-cdr (car city) cities)))))

(defun collect-all-relays-global (list)
  "In a LIST, collect all servers/relays for all cities in all countries."
  (alexandria:flatten
   (loop for country in *mullvad-data*
         collect (let ((cities (find-and-return-cdr (car country) list)))
                   (loop for city in cities
                         collect (find-and-return-cdr (car city) cities))))))

(defun find-location-of-relay (relay)
  "Find the country and city of a RELAY/server."
  (let ((location 'nil))
    (loop for country in *mullvad-data*
          do (loop for city in (cadr country)
                   do (let ((servers (collect-all-relays-for-city
                                      (car country)
                                      (car city)
                                      *mullvad-data*))) ;; master list
                        (loop for server in servers
                              do (when
                                     (string=
                                      (mullvad-translate-relay server) relay)
                                   (setf location (concatenate 'string
                                                               (car country)
                                                               ":"
                                                               (car city))))))))
    relay))

(defun update-argos-status ()
  "Appropriately update the icon and text of Argos extension header."
  (let ((currconnection (get-current-connection)))
    ;; (unless (string= currconnection *last-connection*)
      (if (null currconnection)
          (progn
            (setf *argos-icon* (concatenate 'string "image=" *volemad-disconnected-icon*))
            (setf *argos-status* "VPN off"))
          (progn
            (setf *argos-icon* (concatenate 'string "image=" *volemad-connected-icon*))
            (setf *argos-status* (subseq (find-location-of-relay currconnection) (length "mullvad-")))))
      (setf *last-connection* currconnection)))

(defun argos-create-menu ()
  "Construct menu to send to Argos interface."
  ;; define 'DISCONNECT' menu item for yad
  ;; (setf *yad-country-connect*
  ;;       (concatenate 'string
  (format t (concatenate 'string *argos-status* " | " *argos-icon* "~%"))
  (format t "---~%")
  (format t "DISCONNECT | bash='volemad-cli -d' terminal=false refresh=true~%")
  (format t "---~%")
  (format t "RANDOM server | bash='volemad-cli -r' terminal=false refresh=true~%")
  (format t "---~%")
;; )

  ;; create Yad menu items for different Mullvad relays/servers
  (loop for country in *mullvad-data*
        do (if (> (length (cadr country)) 1) ;; for countries with only one city
               (progn
                 (format t
                  (concatenate 'string 
                               (string-upcase (car country))
                               " | bash='volemad-cli -p " (car country) "'"
                               " terminal=false refresh=true~%"))
                 (loop for city in (cadr country)
                       do (format t
                           (concatenate 'string
                                        "  " (car city) " | bash='volemad -p " 
                                        (car country) " -s "
                                        (car city) "'"
                                        " terminal=false refresh=true~%"))))
               (progn                        ;; for countries with multiple cities
                 (loop for city in (cadr country)
                       do (format t (concatenate 'string 
                                              (string-upcase (car country)) ": "
                                              (car city) " | bash='volemad-cli -p "
                                              (car country) "'"
                                              " terminal=false refresh=true~%")))))))

(defun save-out-relays ()
  (let ((stream (open
                 (progn (ensure-directories-exist (concatenate 'string (uiop:getenv "HOME") "/.cache/volemad/"))
                        (merge-pathnames (concatenate 'string (uiop:getenv "HOME") "/.cache/volemad/") "relays.lisp")) :direction :output :if-exists :supersede :if-does-not-exist :create)))
    (write *mullvad-data* :stream stream )
    (close stream)))

;; like Emacs' executable-find
(defun executable-find (exec)
  "Determine if EXEC is an executable program on system."
  (let ((executable-exists
          (string-trim '(#\Newline #\") (with-output-to-string (s)
                                          (uiop:run-program
                                           (concatenate 'string
                                                        "sh -c \"command -v " exec " 2>&1 \"")
                                           :output s :ignore-error-status t)))))
    (if (not (string= executable-exists ""))
        executable-exists
        nil)))

;; (defun assocvalue (inp alist)
;;   "Helper function to get the 'value' of a 'key' (INP) in an assoc ALIST."
;;   (cdr (assoc inp alist :test #'string=)))

;; (defun argos-updater ()
;;   "Yad tooltip & icon update timer."
;;   (bt:make-thread
;;    (lambda ()
;;      (loop while t
;;            do (sleep 2)
;;               (update-argos-status)))))


(defun menu ()
  ;; (argos-updater)
  (when (null *mullvad-data*)
    (mullvad-parse-json)
    (save-out-relays))
  (update-argos-status)
  (argos-create-menu))

;;  sbcl --eval "(ql:quickload :volemad-argos)" --eval "(progn (in-package :volemad-argos) (sb-ext:save-lisp-and-die \"volemad-argos\" :toplevel #'volemad-argos:menu :executable t :purify t))"
