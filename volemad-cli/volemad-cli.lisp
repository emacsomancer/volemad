;;; volemad-cli.lisp --- cli-interface Mullvad WireGuard connections

;; Copyright (C) 2020 Benjamin Slade

;; LICENCE:
;; This program is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; This program is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
;; PARTICULAR PURPOSE. See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License along with this
;; program. If not, see http://www.gnu.org/licenses/.

(defpackage :volemad-cli
  (:use :common-lisp)
  (:export #:choose))

(in-package :volemad-cli)

;; determine icon locations
(defparameter *volemad-connected-icon* 'nil)
(defparameter *volemad-disconnected-icon* 'nil)
(if (probe-file "/usr/share/icons/volemad/volemad-connected.png")
    (progn
      (setf *volemad-connected-icon* "/usr/share/icons/volemad/volemad-connected.png")
      (setf *volemad-disconnected-icon* "/usr/share/icons/volemad/volemad-disconnected.png"))
    (progn
      (setf *volemad-connected-icon* "/usr/local/share/icons/volemad/volemad-connected.png")
      (setf *volemad-disconnected-icon* "/usr/local/share/icons/volemad/volemad-disconnected.png")))


;; like Emacs' executable-find
(defun executable-find (exec)
  "Determine if EXEC is an executable program on system."
  (let ((executable-exists
          (string-trim '(#\Newline #\") (with-output-to-string (s)
                                          (uiop:run-program
                                           (concatenate 'string
                                                        "sh -c \"command -v " exec " 2>&1 \"")
                                           :output s :ignore-error-status t)))))
    (if (not (string= executable-exists ""))
        executable-exists
        nil)))

(defun assocvalue (inp alist)
  "Helper function to get the 'value' of a 'key' (INP) in an assoc ALIST."
  (cdr (assoc inp alist :test #'string=)))

(defun find-ip-ipapi ()
  "Get information about current (apparent) ip location."
  (let* ((ip (string-trim '(#\Newline #\")
                          (write-to-string
                           (drakma:http-request "https://ipinfo.io/ip"))))
         (stream (drakma:http-request
                  (concatenate 'string "https://ipapi.co/" ip "/json")
                  :want-stream t))
         (geoinfo 'nil)
         (json-error 'nil))
    (setf (flexi-streams:flexi-stream-external-format stream) :utf-8)
    ;; (ignore-errors
    (handler-case 
        (setf geoinfo (yason:parse stream :object-as :alist))
      (error (c)
        (setf json-error 't)))
    (if json-error
        "Error: could not determine ip information"
        (let* ((country (assocvalue "country_name" geoinfo))
               (region (assocvalue "region" geoinfo))
               (city (assocvalue "city" geoinfo))
               (latitude (write-to-string (assocvalue "latitude" geoinfo)))
               (longitude (write-to-string (assocvalue "longitude" geoinfo)))
               (timezone (assocvalue "timezone" geoinfo))
               (ip (assocvalue "ip" geoinfo)))
          (concatenate 'string city ", " region ", " country "
[" timezone "]" "
(" latitude "," longitude ")
" ip)))))

(defun get-current-connection ()
  "Return the current connected Mullvad server (if connected)."
  (when (executable-find "wg")
    (let* ((string1
             (with-output-to-string (s)
               (uiop:run-program "wg show"
                                 :ignore-error-status t
                                 :error-output s))))
      (if (equal string1 "")
          'nil
          (string-trim '(#\Newline #\")
                       (subseq string1
                               (search "mullvad-" string1)
                               (search ": " string1
                                       :start2 (search "mullvad-" string1))))))))

;; create top-level variable to store the last known Mullvad connection
(defparameter *last-connection* "UPDATEME")

(defun mullvad-translate-relay (relay)
  "Translate 'xxx-wireguard' into 'mullvad-xxx'."
  (concatenate 'string "mullvad-"
               (subseq relay 0 (position #\- relay :test #'equal))))

(defun find-and-return-cdr (target list)
  "Helper function to get the cdr of TARGET in LIST."
  (if (equal list 'nil)
      'nil
      (let ((top (car list)))
        (if (equal (car top) target)
            (cadr top)
            (find-and-return-cdr target (cdr list))))))

(defun collect-all-relays-for-city (country city list)
  "In a LIST, collect all servers/relays for CITY in COUNTRY."
  (let ((cities (find-and-return-cdr country list)))
    (find-and-return-cdr city cities)))

(defun collect-all-relays-for-country (country list)
  "In a LIST, collect all servers/relays for (all cities) in COUNTRY."
  (alexandria:flatten
   (let ((cities (find-and-return-cdr country list)))
     (loop for city in cities
           collect (find-and-return-cdr (car city) cities)))))

(defun collect-all-relays-global (list)
  "In a LIST, collect all servers/relays for all cities in all countries."
  (alexandria:flatten
   (loop for country in *mullvad-data*
         collect (let ((cities (find-and-return-cdr (car country) list)))
                   (loop for city in cities
                         collect (find-and-return-cdr (car city) cities))))))

(defun find-location-of-relay (relay)
  "Find the country and city of a RELAY/server."
  (let ((location 'nil))
    (loop for country in *mullvad-data*
          do (loop for city in (cadr country)
                   do (let ((servers (collect-all-relays-for-city
                                      (car country)
                                      (car city)
                                      *mullvad-data*))) ;; master list
                        (loop for server in servers
                              do (when
                                     (string=
                                      (mullvad-translate-relay server) relay)
                                   (setf location (concatenate 'string
                                                               (car country)
                                                               ":"
                                                               (car city))))))))
    (concatenate 'string location " (" relay ")")))

(defun message-daemon (action)
  (let* ((location (uiop:read-file-lines "/tmp/volemad-loc"))
         (to-daemon (open (car location) :direction :output :if-exists :append)))
    (write-line action to-daemon)
    (force-output)
    (finish-output to-daemon)
    (close to-daemon)))

(defun mullvad-disconnect (target-connection)
  "Disconnect from Wireguard TARGET-CONNECTION server/relay."
  (when (executable-find "wg-quick")
    (message-daemon (concatenate 'string "down " target-connection))
    ;; (uiop:run-program
    ;;  (concatenate 'string "sudo wg-quick down " target-connection))
    ))

(defun test-blacklist ()
  "Test if currently connected server is blacklisted."
  (let* ((json-string (map 'string 'code-char
                           (drakma:http-request "https://am.i.mullvad.net/json")))
         (jsparse (jsown:parse json-string))
         (blacklist (jsown:val jsparse "blacklisted"))
         (blacklisted (jsown:val blacklist "blacklisted")))
    blacklisted))

(defun choose-random (list)
  "Choose a random member of LIST."
  (setf *random-state* (make-random-state t))
  (nth (random (length list)) list))

(defun mullvad-connect (servers)
  "Connect to one the Mullvad Wireguard SERVERS."
  (when (executable-find "wg-quick")
    (let ((random-server (choose-random servers)))
      (message-daemon (concatenate 'string "up " random-server))
      ;; (uiop:run-program (concatenate 'string
      ;;                                "sudo wg-quick up "
      ;;                                random-server))
      (sleep 3)
      (let ((currconnection (get-current-connection)))
        (when (test-blacklist) ;; if connected server is blacklisted
          (mullvad-disconnect currconnection)
          ;; disconnect and try another server from the same pool, after
          ;; removing blacklisted server
          (mullvad-connect (remove currconnection servers :test #'string=)))))))

(defun mullvad-action (country city)
  "Given INPUT, decide which relays are appropriate to connect to."
  (let* ((currconnection (get-current-connection))
         (relays
           (if (null country)
               (collect-all-relays-global *mullvad-data*)
               (if (null city)
                   (collect-all-relays-for-country country *mullvad-data*)
                   (collect-all-relays-for-city country city *mullvad-data*))))
         (mullvad-servers
           (loop for relay in relays
                 collect (mullvad-translate-relay relay))))
    (unless (null currconnection)
      (mullvad-disconnect currconnection))
    (mullvad-connect mullvad-servers)))

(defun mullvad-parse-json ()
  "Parse JSON data from Mullvad."
  (let ((json-string (map 'string 'code-char
                          (drakma:http-request
                           "https://api.mullvad.net/public/relays/wireguard/v1/"))))
    (let* ((jsparse (jsown:parse json-string))
           (jscountries (jsown:val jsparse "countries"))
           (servers 'nil)
           (cities-servers 'nil))
      (setf *mullvad-data*
            (loop for country in jscountries
                  do (let ((citylist (jsown:val country "cities")))
                       (setf cities-servers
                             (loop for city in citylist
                                   do (let ((serverlist (jsown:val city "relays")))
                                        (setf servers
                                              (loop for server in serverlist
                                                    collect (jsown:val server "hostname"))))
                                   collect (cons (jsown:val city "name") (list servers)))))
                  collect (cons (jsown:val country "name") (list cities-servers)))))))

(defun save-out-relays ()
  (let ((stream (open
                 (progn (ensure-directories-exist (concatenate 'string (uiop:getenv "HOME") "/.cache/volemad/"))
                        (merge-pathnames (concatenate 'string (uiop:getenv "HOME") "/.cache/volemad/") "relays.lisp")) :direction :output :if-exists :supersede :if-does-not-exist :create)))
    (write *mullvad-data* :stream stream )
    (close stream)))

(defun choose ()
  "Interpret INPUT from Yad menu commands."
  ;; get user directory
  (defparameter *userhome* (uiop:getenv "HOME"))

  (opts:define-opts
    (:name :disconnect
     :description "Disconnect"
     :short #\d)
    (:name :random
     :description "Random"
     :short #\r)     
    (:name :location
     :description "Location"
     :short #\l)
    (:name :forcerebuild
     :description "Force Rebuild"
     :short #\b)
    (:name :country
     :description "Country"
     :short #\p
     :arg-parser #'identity) ;; <- takes an argument
    (:name :city
     :description "City"
     :short #\s
     :arg-parser #'identity) ;; <- takes an argument
    )

  (defparameter *disconnect* 'nil)
  (defparameter *random* 'nil)
  (defparameter *location* 'nil)
  (defparameter *rebuild-mullvad-data* 'nil)
  (defparameter *country* 'nil)
  (defparameter *city* 'nil)
  
  (multiple-value-bind (options)
      (opts:get-opts)
    (cond
      ((getf options :disconnect)
       (setf *disconnect* 't))
      ((getf options :random)
       (setf *random* 't))
      ((getf options :location)
       (setf *location* 't))
      ((getf options :forcerebuild)
       (setf *rebuild-mullvad-data* 't))
      (t (progn
           (when (getf options :country)
             (setf *country* (getf options :country)))
           (when (getf options :city)
             (setf *city* (getf options :city)))))))

  (defparameter *mullvad-data* 'nil)
  (if (probe-file (merge-pathnames
                   (concatenate 'string  (uiop:getenv "HOME")
                                "/.cache/wirevole/relays.lisp")))
      (let ((in (open (merge-pathnames
                       (concatenate 'string  (uiop:getenv "HOME")
                                    "/.cache/wirevole/relays.lisp")))))
        (setf *mullvad-data* (car (list (read in))))
        (close in))
      (progn (mullvad-parse-json)
             (save-out-relays)))
  (cond 
    (*disconnect*
     (let ((currconnection (get-current-connection)))
       (unless (null currconnection)
         (mullvad-disconnect currconnection))))
    (*location*
     (when (executable-find "notify-send")
       (if (null (get-current-connection))
           (uiop:run-program
            (concatenate 'string
                         "notify-send 'Apparent location' '"
                         (find-ip-ipapi) "' --icon=" *volemad-disconnected-icon*))
           (uiop:run-program
            (concatenate 'string
                         "notify-send 'Apparent location' '"
                         (find-ip-ipapi) "' --icon=" *volemad-connected-icon*)))))
    (*rebuild-mullvad-data*
     (progn (mullvad-parse-json)
            (save-out-relays)))
    (*random*
     (mullvad-action 'nil 'nil))
    (t (mullvad-action *country* *city*)))
  (uiop:quit))

;; (menu-action)


;;  sbcl --eval "(ql:quickload :volemad-cli)" --eval "(progn (in-package :volemad-cli) (sb-ext:save-lisp-and-die \"volemad-cli\" :toplevel #'volemad-cli:choose :executable t :purify t))"
