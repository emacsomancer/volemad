;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;;;; volemad-cli.asd

#+sb-core-compression
(defmethod asdf:perform ((o asdf:program-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression 9))

(asdf:defsystem #:volemad-cli
  :description "A commandline interface for Mullvad WireGuard interaction."
  :author "Benjamin Slade <slade@lambda-y.net>"
  :license "GPLv3"
  :depends-on (#:alexandria
               #:drakma
               #:jsown
               #:unix-opts
               #:yason)
  :serial t
  :components ((:file "volemad-cli"))
  ;; :defsystem-depends-on (#:deploy) ;; https://github.com/Shinmera/deploy
  ;; :build-operation "deploy-op"
  :build-operation "program-op"
  :build-pathname "volemad-cli"
  :entry-point "volemad-cli:choose")

;; look at static-program-op & Shinmera's deploy-op
